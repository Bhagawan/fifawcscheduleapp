package com.example.wcschedule.viewmodels

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.lifecycle.MutableLiveData
import androidx.work.ExistingWorkPolicy
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import androidx.work.workDataOf
import com.example.wcschedule.BR
import com.example.wcschedule.data.Fixtures
import com.example.wcschedule.util.NotificationWorker
import com.example.wcschedule.util.ServerClientSchedule
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class MainScheduleViewModel(private val workManager: WorkManager): BaseObservable() {
    private val splashScope = CoroutineScope(Dispatchers.IO)

    val backUrl = "http://195.201.125.8/WCScheduleApp/back.png"

    companion object {
        const val ERROR_NETWORK = 0
    }

    @Bindable
    var schedule = emptyList<Fixtures>()
    val error = MutableLiveData<Int>()

    private var request: Job = splashScope.async {
        val s = ServerClientSchedule.create().getStretch()
        if (s.isSuccessful) {
            s.body()?.let {
                schedule = it
                notifyPropertyChanged(BR.schedule)
                scheduleNotifications(it)
            }
        } else error.postValue(ERROR_NETWORK)
    }

    fun destroy() {
        if(request.isActive) request.cancel()
    }

    private fun scheduleNotifications(schedule: List<Fixtures>) {
        val inputFormatter = SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH)

        val dayDate = Calendar.getInstance()
        for(day in schedule) {
            dayDate.time = inputFormatter.parse(day.day)?: Date()
            for (match in day.matches) {
                val name = match.first_team + " - " + match.second_team

                val mTime = Calendar.getInstance()
                mTime.time = SimpleDateFormat("HH:mm", Locale.getDefault()).parse(match.time)?: Date()
                mTime.set(dayDate.get(Calendar.YEAR), dayDate.get(Calendar.MONTH), dayDate.get(Calendar.DAY_OF_MONTH))

                val delay = System.currentTimeMillis() - mTime.time.time - 3600000
                if(delay > 0) {
                    val matchNotificationRequest = OneTimeWorkRequestBuilder<NotificationWorker>()
                        .setInputData(workDataOf("time" to match.time,"match" to  name))
                        .addTag(name)
                        .setInitialDelay(delay, TimeUnit.MILLISECONDS)
                        .build()
                    workManager.enqueueUniqueWork(name, ExistingWorkPolicy.KEEP, matchNotificationRequest)
                }
            }
        }
    }
}