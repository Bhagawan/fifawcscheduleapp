package com.example.wcschedule.viewmodels

import androidx.databinding.BaseObservable
import com.example.wcschedule.data.Fixtures
import java.text.SimpleDateFormat
import java.util.*

class FixturesViewModel(val fixtures: Fixtures): BaseObservable() {
    private val inputFormatter = SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH)
    private val outputFormatter = SimpleDateFormat("dd MMMM yyyy", Locale("ru", "RU"))
    val day: String

    init {
        val date = inputFormatter.parse(fixtures.day)
        day = if (date != null) outputFormatter.format(date) else fixtures.day
    }
}