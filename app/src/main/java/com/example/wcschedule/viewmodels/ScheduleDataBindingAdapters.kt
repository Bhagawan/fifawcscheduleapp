package com.example.wcschedule.viewmodels

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.wcschedule.adapters.DayAdapter
import com.example.wcschedule.adapters.DynamicAdapter
import com.example.wcschedule.adapters.ScheduleAdapter
import com.squareup.picasso.Picasso
import im.delight.android.webview.AdvancedWebView

@BindingAdapter("recyclerAdapter")
fun initAdapter(recycler: RecyclerView, adapter: String) {
    if(recycler.adapter == null) {
        when(adapter) {
            "schedule" -> {
                recycler.layoutManager = LinearLayoutManager(recycler.context, RecyclerView.HORIZONTAL, false)
                recycler.adapter = ScheduleAdapter()
            }
            "day" -> {
                recycler.layoutManager = LinearLayoutManager(recycler.context)
                recycler.adapter = DayAdapter()
            }
        }
    }
}

@Suppress("UNCHECKED_CAST")
@BindingAdapter("recyclerData")
fun <T> setData(recycler: RecyclerView, data: T) {
    if(recycler.adapter != null){
        if(recycler.adapter is DynamicAdapter<*>) (recycler.adapter as DynamicAdapter<T>).setData(data)
    }
}

@BindingAdapter("imageUrl")
fun loadUrl(view: ImageView, url: String) {
     Picasso.get().load(url).fit().into(view)
}

@BindingAdapter("url")
fun setUrl(v: AdvancedWebView, url: String) {
    v.loadUrl(url)
}


