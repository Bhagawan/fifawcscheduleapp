package com.example.wcschedule.data

import androidx.annotation.Keep

@Keep
data class Fixtures(val day: String, val matches: List<Match>)