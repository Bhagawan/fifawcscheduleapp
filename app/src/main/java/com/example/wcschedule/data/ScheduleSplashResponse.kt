package com.example.wcschedule.data

import androidx.annotation.Keep

@Keep
class ScheduleSplashResponse(val url : String)