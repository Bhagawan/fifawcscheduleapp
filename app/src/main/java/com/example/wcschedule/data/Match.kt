package com.example.wcschedule.data

import androidx.annotation.Keep

@Keep
data class Match(val first_team: String,val first_logo: String,val second_team: String,val second_logo: String,val time: String)
