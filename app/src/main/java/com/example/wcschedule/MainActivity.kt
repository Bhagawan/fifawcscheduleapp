package com.example.wcschedule

import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.work.WorkManager
import com.example.wcschedule.databinding.ActivityMainBinding
import com.example.wcschedule.viewmodels.MainScheduleViewModel
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var target: Target

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val viewModel = MainScheduleViewModel(WorkManager.getInstance(this))
        binding.viewModel = viewModel

        viewModel.error.observe(this) {
            if(it == MainScheduleViewModel.ERROR_NETWORK) sendError(getString(R.string.error_network))
        }
        setContentView(binding.root)
        loadBackground()
    }

    override fun onDestroy() {
        (binding.viewModel as MainScheduleViewModel).destroy()
        super.onDestroy()
    }

    private fun sendError(error: String) = Toast.makeText(this, error, Toast.LENGTH_SHORT).show()

    private fun loadBackground() {
        target = object : Target {
            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                bitmap?.let { binding.root.background = BitmapDrawable(resources, it) }
            }

            override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) { }

            override fun onPrepareLoad(placeHolderDrawable: Drawable?) { }

        }
        Picasso.get().load((binding.viewModel as MainScheduleViewModel).backUrl).into(target)
    }
}