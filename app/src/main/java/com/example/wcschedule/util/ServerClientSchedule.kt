package com.example.wcschedule.util

import com.example.wcschedule.data.Fixtures
import com.example.wcschedule.data.ScheduleSplashResponse
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST

interface ServerClientSchedule {

    @FormUrlEncoded
    @POST("WCScheduleApp/splash.php")
    suspend fun getSplash(@Field("locale") locale: String
                  , @Field("simLanguage") simLanguage: String
                  , @Field("model") model: String
                  , @Field("timezone") timezone: String): Response<ScheduleSplashResponse>

    @GET("WCScheduleApp/fixtures.json")
    suspend fun getStretch(): Response<List<Fixtures>>

    companion object {
        fun create() : ServerClientSchedule {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://195.201.125.8/")
                .build()
            return retrofit.create(ServerClientSchedule::class.java)
        }
    }

}