package com.example.wcschedule.util

import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.example.wcschedule.MainActivity
import com.example.wcschedule.R

class NotificationWorker(appContext: Context, workerParameters: WorkerParameters): Worker(appContext, workerParameters) {

    override fun doWork(): Result {

        val time = inputData.getString("time")?: ""
        val match = inputData.getString("match")?: ""

        return try {
            val manager  = applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val channel = NotificationChannel("1", "WCNotificationChannel", NotificationManager.IMPORTANCE_DEFAULT)
                manager.createNotificationChannel(channel)
            }
            manager.notify(1, createNotification(time, match))
            Result.success()
        } catch (e: Exception) {
            Result.failure()
        }
    }


    @SuppressLint("UnspecifiedImmutableFlag")
    private fun createNotification(time: String, match: String): Notification {
        val builder: NotificationCompat.Builder = NotificationCompat.Builder(applicationContext, "1")
            .setContentTitle("Сегодня в $time пройдет матч")
            .setContentText(match)
            .setSmallIcon(R.drawable.ic_baseline_notifications)
            .setAutoCancel(true)

        val resultIntent = Intent(applicationContext, MainActivity::class.java)
        val resultPendingIntent = PendingIntent.getActivity(applicationContext, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT)
        builder.setContentIntent(resultPendingIntent)
        return builder.build()
    }


}