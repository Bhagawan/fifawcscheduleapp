package com.example.wcschedule.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.example.wcschedule.BR
import com.example.wcschedule.R
import com.example.wcschedule.data.Fixtures
import com.example.wcschedule.viewmodels.FixturesViewModel

class ScheduleAdapter  : RecyclerView.Adapter<ScheduleAdapter.ViewHolder>(), DynamicAdapter<List<Fixtures>> {
    private var items = emptyList<Fixtures>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ViewDataBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_recycler, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int {
        return items.size
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun setData(data: List<Fixtures>) {
        if(items.isEmpty()) {
            items = data
            notifyDataSetChanged()
        }
    }

    inner class ViewHolder(private val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Fixtures) {
            binding.setVariable(BR.viewModel, FixturesViewModel(item))
        }
    }
}