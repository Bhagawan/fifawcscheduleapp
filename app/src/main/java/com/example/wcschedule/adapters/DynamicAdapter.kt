package com.example.wcschedule.adapters

interface DynamicAdapter<T> {
    fun setData(data: T)
}