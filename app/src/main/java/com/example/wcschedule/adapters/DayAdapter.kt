package com.example.wcschedule.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.example.wcschedule.BR
import com.example.wcschedule.R
import com.example.wcschedule.data.Match

class DayAdapter  : RecyclerView.Adapter<DayAdapter.ViewHolder>(), DynamicAdapter<List<Match>> {
    private var items = emptyList<Match>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ViewDataBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_match, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int {
        return items.size
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun setData(data: List<Match>) {
        if (items.isEmpty()) {
            items = data
            notifyDataSetChanged()
        }
    }

    inner class ViewHolder(private val binding: ViewDataBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Match) {
            binding.setVariable(BR.match, item)
        }
    }
}